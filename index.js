require('dotenv').config()
const fs = require('fs'),
  ini = require('ini')
  path = require('path'),
  { execSync } = require('child_process'),
  moment = require('moment')

const filenameRegex = /^wg0-client-(.+)\.conf$/

function getExistingPeers() {
  const peers = []
  const configFilesPath = path.resolve(process.env.PEER_CONFIGS_PATH)
  const files = fs.readdirSync(configFilesPath)
  files.forEach(file => {
    const match = filenameRegex.exec(file)
    if (match) {
      try {
        const config = ini.parse(fs.readFileSync(path.resolve(path.join(configFilesPath, file)), 'utf8'))
        if (config && config['Peer'] && config['Peer']['PublicKey']) {
          peers.push({
            name: match[1],
            publicKey: config['Peer']['PublicKey']
          })
        } else {
          console.log('No peer info found in config file: ' + file)
        }
      } catch (err) {
        console.log(err)
      }
    }
  })
  return peers
}

function getHandshakeInfo() {
  let result = {}
  try {
    const output = execSync(`sudo wg show wg0 latest-handshakes`)
    const parsed = output.toString().split('\n').filter(peer => !!peer).map(peer => peer.split('\t'))
    result = parsed.reduce((acc, item) => {
      acc[item[0]] = new Date(parseInt(item[1]) * 1000)
      return acc
    }, {})
  } catch (error) {
    console.error(`Error executing command: ${error}`);
  }
  return result
}

function getPeersInfo() {
  const peers = getExistingPeers()
  const handshakeInfo = getHandshakeInfo()
  const peersData = peers.map(peer => {
    const lastHandshake = handshakeInfo[peer.publicKey] || null
    const lastHandshakeFormatted = lastHandshake ? moment(lastHandshake).fromNow() : null
    return {
      name: peer.name,
      publicKey: peer.publicKey,
      lastHandshake,
      lastHandshakeFormatted
    }
  })
  console.log(JSON.stringify(peersData, null, 2))
}

getPeersInfo()